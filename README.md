# Saper #

Implementacja prostej gry logicznej za pomocą bibliotek [SFML](https://www.sfml-dev.org/index.php) + [SFGUI](http://sfgui.sfml-dev.de/) (na bazie OpenGL) w standardzie C++14

Użyte wzorce/idiomy:

- RAII [why RAII](http://www.bromeon.ch/articles/raii.html)

- Callback

### Opis modułów programu ###

**Application** - Application.h, Application.cpp

Realizuje podstawowe zadania zewnętrzne:

- tworzy okno
- przekazuje eventy (zdarzenia, np: ruch myszy, klawisze, etc)
- spaja interfejs graficzny użytkownika i logiką gry w jedną całość

**UserInterface** - UserInterface.h, UserInterface.cpp

- umożliwia wpisywanie danych i wybieranie poziomu trudności
- sprawdza poprawność danych (`std::regex`, `std::regex_match`)
- reaguje na odpowiednie zdarzenia, przekazując dane do gry (np wpisaną wielkość planszy)
- rysuje przyciski, ramki, okienka do tekstu itd

**Game** - Game.h, Game.cpp

- odbiera informacje od UI
- zarządza planszą gry
- sprawdza zasady gry

**TileMap** - TileMap.h, TileMap.cpp

- Zawiera `std::vector<Tile>` reprezentujący pola gry
- zarządza układem pól, ustawia im pozycje oraz oblicza ilość sąsiadów
- wykonuje liczne algorytmy i zapytania do tile'ów

**Tile** - Tile.h, Tile.cpp

- Reprezentuje najmniejszą część logiczną gry - 1 pole
- zawiera 4 zmienne
    - czy jest miną
    - czy jest odkryte
    - ile ma sąsiednich min
    - w jaki sposób jest oznaczone przez gracza

**Config** - Config.h, Config.cpp

- przechowuje informacje załadowane z konfiguracji (np xml)
- zapewnia domyślne wartości w przypadku braku pliku konfiguracyjnego

Global.h, Global.cpp

- globalne stałe i funkcje

main.cpp

- zawiera `int main()`
- tworzy obiekt klasy Application
