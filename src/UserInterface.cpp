#include "UserInterface.hpp"

#include "Global.hpp"

UserInterface::UserInterface(Game& game)
	: regex_uint(R"(\s*\d+\s*)"),
	  regex_fraction(R"(\s*\+?\d+(\.?\d+(([eE]([-+])?)?\d+)?)?\s*)"),
	  game(game),
	  board_size{0, 0},
	  mines_amount(0),
	  density(0)
{
	// widgets creation
	m_window = sfg::Window::Create(sfg::Window::Style::BACKGROUND);
	m_window->SetTitle(sf::String("Menu"));
		m_box_main = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f);
			m_box_top = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 5.f);

				m_button_new_game = sfg::Button::Create(sf::String("New Game"));

				m_frame_difficulty = sfg::Frame::Create(sf::String("Difficulty"));
					m_box_difficulty = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 5.f);
						m_button_difficulty_easy   = sfg::ToggleButton::Create(sf::String("easy"));
						m_button_difficulty_medium = sfg::ToggleButton::Create(sf::String("medium"));
						m_button_difficulty_hard   = sfg::ToggleButton::Create(sf::String("hard"));

				m_frame_size = sfg::Frame::Create(sf::String("Board size"));
					m_table_size = sfg::Table::Create();
						m_label_size_x = sfg::Label::Create(sf::String("width"));
						m_label_size_y = sfg::Label::Create(sf::String("height"));
						m_entry_size_x = sfg::Entry::Create();
						m_entry_size_y = sfg::Entry::Create();

				m_frame_density = sfg::Frame::Create(sf::String("Mine density"));
					m_table_density = sfg::Table::Create();
						m_label_mines        = sfg::Label::Create(sf::String("mines"));
						m_label_density      = sfg::Label::Create(sf::String("density"));
						m_entry_mines_amount = sfg::Entry::Create();
						m_entry_density      = sfg::Entry::Create();
						m_label_percent_sign = sfg::Label::Create(sf::String("%"));

				m_frame_current_game = sfg::Frame::Create(sf::String("Current game"));
					m_table_current_game = sfg::Table::Create();
						m_label_flags_used_text    = sfg::Label::Create(sf::String("flags used: "));
						m_label_time_playing_text  = sfg::Label::Create();
						m_label_flags_used_value   = sfg::Label::Create();
						m_label_time_playing_value = sfg::Label::Create();

			m_canvas_game = sfg::Canvas::Create(true);

	// Setting up relationship
	m_desktop.Add(m_window);
		m_window->Add(m_box_main);
			m_box_main->Pack(m_box_top, false, true);

				m_box_top->Pack(m_button_new_game, true, true);

				m_box_top->Pack(m_frame_difficulty, false, true);
					m_frame_difficulty->Add(m_box_difficulty);
						m_box_difficulty->Pack(m_button_difficulty_easy,   true, true);
						m_box_difficulty->Pack(m_button_difficulty_medium, true, true);
						m_box_difficulty->Pack(m_button_difficulty_hard,   true, true);

				m_box_top->Pack(m_frame_size, true, true);
					m_frame_size->Add(m_table_size);
						m_table_size->Attach(m_label_size_x, sf::Rect<sf::Uint32>( 0, 0, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );
						m_table_size->Attach(m_label_size_y, sf::Rect<sf::Uint32>( 0, 1, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );
						m_table_size->Attach(m_entry_size_x, sf::Rect<sf::Uint32>( 1, 0, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );
						m_table_size->Attach(m_entry_size_y, sf::Rect<sf::Uint32>( 1, 1, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );

				m_box_top->Pack(m_frame_density, true, true);
					m_frame_density->Add(m_table_density);
						m_table_density->Attach(m_label_mines,        sf::Rect<sf::Uint32>( 0, 0, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );
						m_table_density->Attach(m_label_density,      sf::Rect<sf::Uint32>( 0, 1, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );
						m_table_density->Attach(m_entry_mines_amount, sf::Rect<sf::Uint32>( 1, 0, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );
						m_table_density->Attach(m_entry_density,      sf::Rect<sf::Uint32>( 1, 1, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );
						m_table_density->Attach(m_label_percent_sign, sf::Rect<sf::Uint32>( 2, 1, 1, 1 ), 0, sfg::Table::FILL );

				m_box_top->Pack(m_frame_current_game, true, true);
					m_frame_current_game->Add(m_table_current_game);
						m_table_current_game->Attach(m_label_flags_used_text,    sf::Rect<sf::Uint32>( 0, 0, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );
						m_table_current_game->Attach(m_label_time_playing_text,  sf::Rect<sf::Uint32>( 0, 1, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );
						m_table_current_game->Attach(m_label_flags_used_value,   sf::Rect<sf::Uint32>( 1, 0, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );
						m_table_current_game->Attach(m_label_time_playing_value, sf::Rect<sf::Uint32>( 1, 1, 1, 1 ), sfg::Table::FILL | sfg::Table::EXPAND, sfg::Table::FILL | sfg::Table::EXPAND );

			m_box_main->Pack(m_canvas_game, true, true);

	// adjustments - setting minimal size for buttons and canvas
	m_button_difficulty_easy  ->SetRequisition({ 96.0f,  32.0f});
	m_button_difficulty_medium->SetRequisition({ 96.0f,  32.0f});
	m_button_difficulty_hard  ->SetRequisition({ 96.0f,  32.0f});
	m_canvas_game             ->SetRequisition({128.0f, 128.0f});

	// binding functionality
	m_button_new_game->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&UserInterface::newGame, this));

	m_entry_size_x->GetSignal(sfg::Entry::OnText).Connect(std::bind(&UserInterface::changeSizeX, this));
	m_entry_size_y->GetSignal(sfg::Entry::OnText).Connect(std::bind(&UserInterface::changeSizeY, this));

	m_entry_mines_amount->GetSignal(sfg::Entry::OnText).Connect(std::bind(&UserInterface::changeMinesAmount, this));
	m_entry_density     ->GetSignal(sfg::Entry::OnText).Connect(std::bind(&UserInterface::changeDensity, this));

	m_button_difficulty_easy  ->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&UserInterface::setUpDifficulty, this, GameDifficulty::easy));
	m_button_difficulty_medium->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&UserInterface::setUpDifficulty, this, GameDifficulty::medium));
	m_button_difficulty_hard  ->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&UserInterface::setUpDifficulty, this, GameDifficulty::hard));

	m_button_difficulty_medium->SetActive(true);
	setUpDifficulty(GameDifficulty::medium);
}

void UserInterface::handleEvent(const sf::Event& event)
{
	m_desktop.HandleEvent(event);
}

void UserInterface::update(sf::Time time)
{
	m_label_flags_used_value  ->SetText(game.getFlagsStatus());
	m_label_time_playing_value->SetText(game.getPlayingTime());

	if (game.isFinished())
		m_label_time_playing_text->SetText(sf::String("time played: "));
	else
		m_label_time_playing_text->SetText(sf::String("playing time: "));

	m_desktop.Update(time.asSeconds());
}

void UserInterface::draw(sf::RenderWindow& window)
{
	// set canvas active for current OpenGL rendering
	m_canvas_game->Bind();
	m_canvas_game->Clear(constants::sfgui_background, true);
	game.draw(m_canvas_game);
	m_canvas_game->Display();
	m_canvas_game->Unbind();

	// set window active for current OpenGL rendering
	window.setActive(true);
	m_sfgui.Display(window);
}

void UserInterface::resizeWindow(sf::Vector2u new_size)
{
	// Tell SFGUI how big it should be
	m_window->SetRequisition(sf::Vector2f(new_size.x, new_size.y));
	// Tell SFGUI to reset the current size and position
	m_window->SetAllocation(sf::FloatRect(0, 0, 0, 0));
	// Tell SFGUI to recalculate
	m_desktop.Refresh();
}

sfg::Canvas::Ptr UserInterface::getCanvasPtr() const
{
	return m_canvas_game;
}

void UserInterface::newGame()
{
	sf::Vector2u size(getValFromEntry(m_entry_size_x), getValFromEntry(m_entry_size_y));
	unsigned mines = getValFromEntry(m_entry_mines_amount);

	game.newGame(size, mines);
}

unsigned UserInterface::getValFromEntry(const sfg::Entry::Ptr& entry) const
{
	std::string input = entry->GetText();

	if (!std::regex_match(input, regex_uint))
	{
		return 0;
	}

	// int because strtoul would underflow unsigned if user inputs negative value
	int val = std::strtol(input.c_str(), nullptr, 10);

	if (errno != 0 or val < 0)
	{
		errno = 0;
		return 0;
	}

	return val;
}

void UserInterface::changeSizeX()
{
	board_size.x = getValFromEntry(m_entry_size_x);
	refillDensity();
	deactivateDifficultyButtons();
}

void UserInterface::changeSizeY()
{
	board_size.y = getValFromEntry(m_entry_size_y);
	refillDensity();
	deactivateDifficultyButtons();
}

void UserInterface::changeMinesAmount()
{
	mines_amount = getValFromEntry(m_entry_mines_amount);
	refillDensity();
	deactivateDifficultyButtons();
}

void UserInterface::changeDensity()
{
	std::string input = m_entry_density->GetText();

	// replace first occurence of , to . (if there are any further string is invalid anyway)
	std::size_t comma_pos = input.find(',');
	if (comma_pos != std::string::npos)
		input[comma_pos] = '.';

	// sets density to 0 if the input string has invalid format
	density = [&]() -> double
	{
		if (!std::regex_match(input, regex_fraction))
			return 0;

		double converted_val = std::strtod(input.c_str(), nullptr);

		if (errno != 0)
		{
			errno = 0;
			return 0;
		}

		return converted_val;
	}();

	if (density != 0)
		density /= 100.0; // input is in %

	refillMinesAmount(); // refill even for 0% density - game will raise appropriate errors
	deactivateDifficultyButtons();
}


void UserInterface::refillDensity()
{
	if (board_size.x == 0 or board_size.y == 0)
		density = 0;
	else
		density = static_cast<double>(mines_amount) / static_cast<double>(board_size.x * board_size.y);

	m_entry_density->SetText(std::to_string(density * 100.0));
}

void UserInterface::refillMinesAmount()
{
	mines_amount = density * static_cast<double>(board_size.x * board_size.y);
	m_entry_mines_amount->SetText(std::to_string(mines_amount));
}

void UserInterface::setUpDifficulty(GameDifficulty game_difficulty)
{
	const DifficultyData& data = constants::difficulty_data[static_cast<std::size_t>(game_difficulty)];
	std::tie(board_size.x, board_size.y, mines_amount) = std::tie(data.size_x, data.size_y, data.mines);

	m_entry_size_x->SetText(std::to_string(board_size.x));
	m_entry_size_y->SetText(std::to_string(board_size.y));
	m_entry_mines_amount->SetText(std::to_string(mines_amount));
	refillDensity();

	deactivateDifficultyButtons();

	if      (game_difficulty == GameDifficulty::easy)
		m_button_difficulty_easy  ->SetActive(true);
	else if (game_difficulty == GameDifficulty::medium)
		m_button_difficulty_medium->SetActive(true);
	else if (game_difficulty == GameDifficulty::hard)
		m_button_difficulty_hard  ->SetActive(true);

	if (game.isFirstMove())
		newGame();
}

void UserInterface::deactivateDifficultyButtons()
{
	m_button_difficulty_easy  ->SetActive(false);
	m_button_difficulty_medium->SetActive(false);
	m_button_difficulty_hard  ->SetActive(false);
}
