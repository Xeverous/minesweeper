#ifndef GAME_HPP_
#define GAME_HPP_
#include <SFGUI/Canvas.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <vector>
#include "TileMap.hpp"

/**
 * @brief core game class
 *
 * runs according to the game logic, verifies user reactions
 * uses very simple FSM pattern
 */
class Game
{
public:
	Game();
	~Game() = default;
	/**
	 * @brief Updates game object
	 *
	 * @param time time passed between frames
	 */
	void update(sf::Time time);
	/**
	 * @brief Event handling function
	 *
	 * basing on canvas properties and mouse position calculates position in the game's coordinate system
	 * @param event event that ocurred in the system
	 * @param mouse_position mouse position in the window coordinates
	 * @param canvas pointer to the canvas game is being drawn to
	 */
	void handleEvent(const sf::Event& event, sf::Vector2i mouse_position, sfg::Canvas::Ptr canvas);
	/**
	 * @brief Draws the game onto the canvas
	 * @param canvas pointer to the canvas game to draw to
	 */
	void draw(sfg::Canvas::Ptr canvas);
	/**
	 * @brief Creates new tile layout, then shuffles it (uniformly)
	 *
	 * @param new_size new game size, both x and y values must be > 0
	 * @param mines amount of mines to place on the board, must be < new_size.x * new_size.y
	 *
	 * if input values are incorrect setups appropriate annoucement
	 */
	void newGame(sf::Vector2u new_size, unsigned mines);
	/**
	 * @brief checks if user has performed first move
	 *
	 * @return true if first was move was not performed
	 */
	bool isFirstMove() const;
	/**
	 * @brief check if game is finished
	 *
	 * @return true if game is either win or lost, false if still playing
	 */
	bool isFinished() const;
	/**
	 * @brief check if game is being played
	 *
	 * @return true if user has performed any action
	 */
	bool isPlaying() const;
	/**
	 * @brief get the time game is being played
	 *
	 * @return time as string, with appended unit (s or min) or string "-" if game has not been started
	 */
	std::string getPlayingTime() const;
	/**
	 * @brief get the status of the flags
	 * @return "flags_placed / mines" or "-" if game has not been started
	 */
	std::string getFlagsStatus() const;

private:
	/**
	 * @brief recalculates view for tile map (it's size, center and viewport)
	 *
	 * @param canvas_size size of the canvas game is being drawn to
	 */
	void recalculateTileMapView(sf::Vector2f canvas_size);
	/**
	 * @brief internal event handling
	 *
	 * performs appropriate actions depending on game's state
	 * @param event event that happened in the system
	 * @param mouse_position position of the mouse, in the game's coordinate system
	 * @sa handleEvent(const sf::Event&, sf::Vector2i, sfg::Canvas::Ptr)
	 */
	void handleEvent(const sf::Event& event, sf::Vector2f mouse_position);
	/**
	 * @brief start revealing from given position
	 *
	 * if a mine is hit, checks if it's first move - if so mine is shuffled with random other non-mine tile,
	 * otherwise function will trigger game loss
	 *
	 * @param position position in tile coordinate system
	 * @warning position must be valid
	 */
	void startRevealing(sf::Vector2u position);
	/**
	 * @brief place flag/question mark on given position and check game's winning condition
	 *
	 * @param position position in tile coordinate system
	 * @warning position must be valid
	 *
	 * @note if there are no more flags, function will set up appropriate annoucement
	 */
	void flagAndCheck(sf::Vector2u position);
	/** @brief enum type for annoucements displayed during gameplay */
	enum class Annoucement
	{
		none,
		won,
		lost,
		invalid_width,
		invalid_height,
		invalid_mines,
		out_of_flags,

		MAX_ANNOUCEMENTS
	};
	/** @brief enum type for storing current game state */
	enum class GameState
	{
		menu,
		playing,
		won,
		lost,
		paused
	};

	bool first_move;           /**< whether player has performed first move */
	unsigned total_mines;      /**< total amount of mines that the current game has */
	unsigned flags_placed;     /**< amount of flags placed by the player */

	GameState state;           /**< current game state */
	sf::Time time_playing;     /**< play time of the game, always >= 0 */

	TileMap tile_map;
	sf::View tile_map_view;    /**< view used to render tile map */

	sf::Font font;             /**< font used for annoucement text */
	sf::Text text;             /**< annoucement text */
	Annoucement annoucement;
	sf::Time annoucement_time; /**< time left to display annoucement, decremented each frame */

	/**
	 * @brief triggers game loss
	 *
	 * sets annoucement, state and reveals entire board
	 */
	void setUpGameLoss();
	/**
	 * @brief sets annoucement text for the player
	 *
	 * the old annoucement (if any) is replaced
	 */
	void setUpAnnoucement(Annoucement a);
	/**
	 * @brief get the annoucement string
	 *
	 * @param a enum value describing desired message
	 * @return string holding the text of desired message
	 */
	std::string getAnnoucementString(Annoucement a) const;
};

#endif /* GAME_HPP_ */
