#include "Tile.hpp"

Tile::Tile(bool is_mine)
	: is_mine(is_mine),
	  is_revealed(false),
	  neighbour_mines(0),
	  flag_state(FlagState::none)
{
}

bool Tile::isSafe() const
{
	return neighbour_mines == 0;
}

bool Tile::isMine() const
{
	return is_mine;
}

bool Tile::isFlagged() const
{
	return flag_state == FlagState::flag;
}

bool Tile::isQuestioned() const
{
	return flag_state == FlagState::question_mark;
}

bool Tile::isUnmarked() const
{
	return flag_state == FlagState::none;
}

bool Tile::isOnlyNumber() const
{
	return neighbour_mines and !isMine();
}

bool Tile::isRevealed() const
{
	return is_revealed;
}

bool Tile::isCorrectlyFlagged() const
{
	return isMine() == isFlagged();
}

unsigned Tile::getNeighbourMinesAmount() const
{
	return neighbour_mines;
}

void Tile::setMineNeighbours(unsigned value)
{
	neighbour_mines = value;
}

void Tile::flag()
{
	if (isRevealed())
		return;

	if (flag_state == FlagState::none)
		flag_state = FlagState::flag;
	else if (flag_state == FlagState::flag)
		flag_state = FlagState::question_mark;
	else if (flag_state == FlagState::question_mark)
		flag_state = FlagState::none;
}

void Tile::reveal(bool check_if_flagged)
{
	if (check_if_flagged)
		if (isFlagged())
			return;

	is_revealed = true;
}

unsigned Tile::getImageIndex() const
{
	if (isRevealed())
	{
		if (isMine())
		{
			if (flag_state == FlagState::none)
				return static_cast<unsigned>(ImageIndex::mine_triggered); // player has not found this mine
			else
				return static_cast<unsigned>(ImageIndex::mine); // player has found this mine
		}
		else
		{
			if (flag_state == FlagState::flag) // player has incorrectly flagged this tile
				return static_cast<unsigned>(ImageIndex::mine_mistake);
			else
				return neighbour_mines;
		}
	}
	else // select image based on the flag - only this can be shown to user
	{
		if (flag_state == FlagState::flag)
			return static_cast<unsigned>(ImageIndex::flag);
		else if (flag_state == FlagState::question_mark)
			return static_cast<unsigned>(ImageIndex::question_mark);
		else // if (flag_state == FlagState::none)
			return static_cast<unsigned>(ImageIndex::hidden);
	}
}
