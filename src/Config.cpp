#include "Config.hpp"

#include "Global.hpp"

sf::Vector2u Config::getWindowSize() const
{
	return constants::startup_window_size;
}
