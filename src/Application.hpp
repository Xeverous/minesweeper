#ifndef APPLICATION_HPP_
#define APPLICATION_HPP_
#include <SFML/Graphics.hpp>
#include <memory>
#include "Config.hpp"
#include "UserInterface.hpp"

/**
 * @brief core program class
 *
 * Manages the UI, window, game object, propagates events and updates
 */
class Application
{
private:
	Game game;
	UserInterface ui;

	Config config;
	std::unique_ptr<sf::RenderWindow> render_window;

	/**
	 * @brief creates a new window based on the configuration
	 */
	void initRender();
	/**
	 * @brief core program loop
	 *
	 * propagates events and updates
	 */
	void gameLoop();
	/**
	 * @brief update funtion
	 *
	 * propagates time passed between frames to all objects
	 */
	void update(sf::Time time);
	/**
	 * @brief event handling funtion
	 *
	 * propagates system (mouse, keyboard, etc) events to all objects
	 */
	void handleEvent(const sf::Event& event);
	/**
	 * @brief draws game and UI onto the window
	 */
	void draw();

public:
	Application();
	/**
	 * @brief laucnhes the program
	 *
	 * @return exit code for the OS
	 */
	int run();
};

#endif /* APPLICATION_HPP_ */
