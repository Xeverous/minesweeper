#ifndef TILEMAP_HPP_
#define TILEMAP_HPP_
#include <SFGUI/Canvas.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <type_traits>
#include "Tile.hpp"

/**
 * @brief class for manipulating the board
 *
 * allows multiple operations to be performed recursively or on certain position
 */
class TileMap : public sf::Transformable
{
public:
	/**
	 * @brief constructor
	 *
	 * initializes vertex array to hold quads
	 */
	TileMap();
	/**
	 * @brief load spritesheet file
	 *
	 * @param spritesheet_path path to the file, preferably png or bmp
	 *
	 * this function can be used to change skins at runtime
	 */
	void load(const std::string& spritesheet_path);
	/**
	 * @brief removes all tiles and creates new board
	 *
	 * @param new_size valid new size (x, y) > 0
	 * @param total_mines valid amount of mines to place, mist be < x*y
	 */
	void reset(sf::Vector2u new_size, unsigned total_mines);
	/**
	 * @brief updates the object
	 *
	 * @param time time passed between frames
	 */
	void update(sf::Time time);
	/**
	 * @brief get requested size to draw
	 *
	 * @return size that rendering of this object will occupy
	 *
	 * value can be used to set view size
	 */
	sf::Vector2f getScreenRequisition() const;
	/**
	 * @brief draw the map
	 *
	 * @param canvas pointer to the canvas to draw to
	 * @param states how to render object
	 */
	void draw(sfg::Canvas::Ptr canvas, sf::RenderStates states);
	/**
	 * @brief compute 2D tile index baseing on mouse position
	 *
	 * @return array position of the hovered tile
	 *
	 * returned position can be passed to getTileByPosition()
	 * example: for (103.7, 25.1) and tile size of 32x32 returns (3, 0)
	 *
	 * @note returns invalid values if mouse is outside canvas
	 */
	sf::Vector2u getTilePosition(const sf::Vector2f& mouse_pos) const;
	/**
	 * @brief check if given position is valid
	 *
	 * @param position position to check
	 *
	 * @return true if position is valid
	 */
	bool isPositionValid(const sf::Vector2u& position) const;
	/**
	 * @brief get access to a tile located at given position
	 *
	 * @return reference to the tile
	 *
	 * @note position must be valid
	 *
	 * @sa isPositionValid()
	 */
	Tile& getTileByPosition(const sf::Vector2u& position);
	/**
	 * @brief get access to a tile located at given position
	 *
	 * @return const reference to the tile
	 *
	 * @note position must be valid
	 *
	 * @sa isPositionValid()
	 */
	const Tile& getTileByPosition(const sf::Vector2u& position) const;
	/**
	 * @brief check if every mine is flagged correctly and every non-mine is not flagged
	 *
	 * this function is Minesweeper's game win condition
	 */
	bool verifyFlagsCorrectness() const;
	/**
	 * @brief check if there is at least 1 tile that is mien and is revealed
	 *
	 * this function is Minesweeper's game loss condition
	 */
	bool isMineRevealed() const;
	/**
	 * @brief check that tile at given position has flags placed around equal to it's amount of mine neighbours
	 *
	 * this function can be used to automatically reveal neighbour tiles if all mines around one point are found
	 */
	bool isTileSatisfied(const sf::Vector2u& position) const;
	/**
	 * @brief forcibly reveal all tiles
	 *
	 * usually called when the game is over
	 */
	void revealAllTiles();
	/**
	 * @brief reveal tile at position, if this tile is safe, reveal it's neighbours
	 *
	 * does nothing if position is invalid
	 *
	 * this function uses recursivelyRevealNeighbours()
	 */
	 // search use
	void recursivelyReveal(const sf::Vector2u& position);
	/**
	 * @brief reveal all neighbours of the tile at given position
	 */
	// search use
	void recursivelyRevealNeighbours(const sf::Vector2u& position);
	/**
	 * @brief swap tile at position with randomly choosen other that is not a mine
	 *
	 * @param position position of the mine to swap to non-mine tile
	 *
	 * can be used to secure player from failing game at first move
	 */
	void swapTileToNonMine(sf::Vector2u position);

private:
	/**
	 * @brief applies correct texture fragment for each tile, depending on it's state
	 *
	 * called at the start of draw()
	 */
	void recalculateTexturePositions();
	/**
	 * @brief recalculates position on the screen for each tile
	 *
	 * needed to be called only when tiles are created or swapped
	 */
	void recalculateScreenPositions();
	/**
	 * @brief get pointer to the tile's rendering vertex
	 *
	 * @param position valid tile position
	 */
	sf::Vertex* getVertexByPosition(const sf::Vector2u& position);
	/**
	 * @brief set appropriate amount of neighbour mines for each tile
	 *
	 * @sa calculateMineNeighbours(const sf::Vector2u&)
	 */
	void calculateMineNeighbours();
	/**
	 * @brief set appropriate amount of neighbour mines for each tile
	 *
	 * @param position valid tile position
	 *
	 * @sa calculateMineNeighbours()
	 */
	void calculateMineNeighbours(const sf::Vector2u& position);
	/**
	 * @brief call lambda for each neighbour of the given position
	 *
	 * @param position position of which to calculate neighbour positions
	 * @param lambda labda expression maching signature `void (const sf::Vector2u&)` that will be called for all possible neighbours
	 *
	 * @note lambda is responsible for handling potentially invalid (out of bounds) neighbour positions
	 */
	template<class Lambda>
	void doForEachNeighbour(const sf::Vector2u& position, Lambda&& lambda);
	/**
	 * @brief sum up return values of lambda calls for each neighbour of the given position
	 *
	 * @param position position of which to calculate neighbour positions
	 * @param lambda labda expression maching signature `T(*)(const sf::Vector2u&)` (where T is an unsigned integral type) that will be called for all possible neighbours
	 *
	 * @note lambda is responsible for handling potentially invalid (out of bounds) neighbour positions
	 */
	template<class Lambda>
	unsigned countForAllNeighbours(const sf::Vector2u& position, Lambda&& lambda) const;

	sf::Vector2u board_size;
	std::vector<Tile> tiles;

	sf::Texture texture;
	sf::VertexArray vertices;
};

template<class Lambda>
void TileMap::doForEachNeighbour(const sf::Vector2u& position, Lambda&& lambda)
{
	// upper
	lambda({position.x - 1, position.y - 1});
	lambda({position.x    , position.y - 1});
	lambda({position.x + 1, position.y - 1});

	// left & right
	lambda({position.x - 1, position.y    });
	lambda({position.x + 1, position.y    });

	// lower
	lambda({position.x - 1, position.y + 1});
	lambda({position.x    , position.y + 1});
	lambda({position.x + 1, position.y + 1});
}

template<class Lambda>
unsigned TileMap::countForAllNeighbours(const sf::Vector2u& position, Lambda&& lambda) const
{
	// make sure that valid type is passed to the template, this type trait requires C++17
	// static_assert(std::is_invocable< Lambda >::value, "Insert any invocable type");
	// https://stackoverflow.com/questions/7943525/is-it-possible-to-figure-out-the-parameter-type-and-return-type-of-a-lambda

	return
		// upper
		lambda({position.x - 1, position.y - 1}) +
		lambda({position.x    , position.y - 1}) +
		lambda({position.x + 1, position.y - 1}) +

		// left & right
		lambda({position.x - 1, position.y    }) +
		lambda({position.x + 1, position.y    }) +

		// lower
		lambda({position.x - 1, position.y + 1}) +
		lambda({position.x    , position.y + 1}) +
		lambda({position.x + 1, position.y + 1});
}

#endif /* TILEMAP_HPP_ */
