#include "Global.hpp"

#include <algorithm>

// because GCC implements std::random_device as deterministic seed it with std::time(nullptr)
std::mt19937 rng(std::time(nullptr));

sf::Vector2f scaleAndFit(const sf::Vector2f& vector, const sf::Vector2f& max_size)
{
	float min_proportion = std::min(max_size.x / vector.x, max_size.y / vector.y);

	return {
		min_proportion * vector.x,
		min_proportion * vector.y
	};
}

sf::IntRect getViewport(const sf::View& view, const sf::Vector2f& size)
{
    const sf::FloatRect& viewport = view.getViewport();

	return {
		static_cast<int>(0.5f + size.x * viewport.left),
		static_cast<int>(0.5f + size.y * viewport.top),
		static_cast<int>(0.5f + size.x * viewport.width),
		static_cast<int>(0.5f + size.y * viewport.height)
	};
}

sf::Vector2f mapPixelToCoords(const sf::Vector2i& point, const sf::View& view, const sf::Vector2f& size)
{
	// First, convert from viewport coordinates to homogeneous coordinates
	sf::IntRect viewport = getViewport(view, size);

	sf::Vector2f normalized {
		-1.0f + 2.0f * (point.x - viewport.left) / viewport.width,
		 1.0f - 2.0f * (point.y - viewport.top ) / viewport.height
	};

	// Then transform by the inverse of the view matrix
	return view.getInverseTransform().transformPoint(normalized);
}
