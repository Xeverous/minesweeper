#ifndef CONFIG_HPP_
#define CONFIG_HPP_
#include <SFML/Graphics.hpp>

/**
 * @brief stores configuration
 *
 * @todo remove placeholder code for actual config file (perhaps xml?)
 */
class Config
{
public:
	/**
	 * @brief Get the size of the window
	 *
	 * @return size loaded from configuration file or default size
	 */
	sf::Vector2u getWindowSize() const;
};

#endif /* CONFIG_HPP_ */
