#ifndef TILE_HPP_
#define TILE_HPP_
/**
 * @brief represents one tile
 *
 * a tile is the most basic object in the Minesweeper game
 * each tile can be:
 * - mine
 * - marked (by flag or question mark)
 * - hidden or revealed
 */
class Tile
{
public:
	/**
	 * @brief constructor
	 *
	 * @param is_mine set whether this tile is a mine or not
	 * @warning it is required to set mine neighbours after creation - each
	 * tile does not know where it is placed and has no access to neighbours
	 */
	Tile(bool is_mine);

	/**
	 * @brief check if this tile has no mine neighbours
	 *
	 * @return true if there are no mine neighbours
	 */
	bool isSafe() const;
	/**
	 * @brief check if this tile is mine
	 *
	 * @return true if so
	 */
	bool isMine() const;
	/**
	 * @brief check if this tile is marked by a flag
	 *
	 * @return true if so
	 */
	bool isFlagged() const;
	/**
	 * @brief check if this tile is marked by a question mark
	 *
	 * @return true if so
	 */
	bool isQuestioned() const;
	/**
	 * @brief check if this tile has no mark
	 *
	 * @return true if unmarked
	 */
	bool isUnmarked() const;
	/**
	 * @brief check if this tile hides only a number
	 *
	 * @return true if there are >= 1 neighbour mines and this tile is not mine, false otherwise
	 */
	bool isOnlyNumber() const;
	/**
	 * @brief check if this tile is visible (player has revealed it)
	 *
	 * @return true if visible
	 */
	bool isRevealed() const;
	/**
	 * @brief check if tile is correctly flagged
	 *
	 * @return true if tile is both mine and flagged
	 *         true if tile is not mine and has no flag
	 *         false otherwise
	 */
	bool isCorrectlyFlagged() const;
	/**
	 * @brief get amount of neighbour mines
	 *
	 * @return amount of neighbour mines
	 */
	unsigned getNeighbourMinesAmount() const;
	/**
	 * @brief set amount of neighbour mines
	 *
	 * @param value amount of neighbour mines
	 */
	void setMineNeighbours(unsigned value);
	/**
	 * @brief cycle through possible flag states
	 *
	 * cycles none => flag => question_mark => none -> ...
	 */
	void flag();
	/**
	 * @brief reveals the tile
	 *
	 * @param check_if_flagged block revealing if tile is flagged
	 *
	 * by default revealing with flag on is blocked so that the player won't accidentally reveal an already flagged mine
	 */
	void reveal(bool check_if_flagged = true);
	/**
	 * @brief get index of the spritesheet image
	 *
	 * @return index of the image in spritesheet that should be rendered for this tile
	 */
	unsigned getImageIndex() const;

private:
	/**
	 * @brief represents information which tile from spritesheet to use when rendering each game's tile
	 *
	 * @note the order of tiles in spritesheet file must match with this enum
	 *
	 * @note spritesheet has no restrictions on it's x/y ratio (both 2x8 and 4x4 will work), last tiles can be empty
	 */
	enum class ImageIndex : unsigned char
	{
		hidden = 9,
		mine_triggered, /**< @brief displayed only when the game has finished */
		mine,           /**< @brief displayed only when the game has finished */
		mine_mistake,   /**< @brief displayed only when the game has finished */
		question_mark,
		flag
	};
	/**
	 * @brief represents all possible flags a mine can have
	 */
	enum class FlagState : unsigned char
	{
		none,
		flag,
		question_mark
	};

	/**
	 * @defgroup TilePrivate private fields
	 * @{
	 */
	bool is_mine;             /**< @brief whether a tile is mine or not */
	bool is_revealed;         /**< @brief whether a tile is revealed or not */
	/**
	 * @brief amount of neighbour mines this tile has
	 *
	 * valid range is [0, 8]
	 */
	unsigned neighbour_mines;
	FlagState flag_state;     /**< @brief which mark (or none) is placed on the tile */
	/* @} */ // end of group
};

#endif /* TILE_HPP_ */
