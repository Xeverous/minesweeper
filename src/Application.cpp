#include "Application.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <exception>
#include "Global.hpp"
#include "UserInterface.hpp"

Application::Application(): ui(game)
{

}

void Application::initRender()
{
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	render_window = std::make_unique<sf::RenderWindow>
	(
		sf::VideoMode(config.getWindowSize().x, config.getWindowSize().y, sf::VideoMode::getDesktopMode().bitsPerPixel),
		"Minesweeper",
		sf::Style::Default,
		settings
	);

	render_window->setVerticalSyncEnabled(true);

	// required by SFGUI
	render_window->resetGLStates();
	ui.update(sf::Time::Zero);
	ui.resizeWindow(render_window->getSize());
}

int Application::run()
{
	try
	{
		initRender();
		gameLoop();
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << '\n';
		return 1;
	}
	catch (...)
	{
		std::cout << "Unknown runtime error\n";
		return 2;
	}

	return 0;
}

void Application::gameLoop()
{
	// could be changed to std::chrono::time_point / ::duration<> but SFML/SFGUI relies heavy on it's own time class
	sf::Clock clock;

	while (render_window->isOpen())
	{
		update(clock.restart());

		sf::Event event;
		while (render_window->pollEvent(event))
			handleEvent(event);

		draw();
	}
}

void Application::update(sf::Time time)
{
	game.update(time);
	ui.update(time);
}

void Application::handleEvent(const sf::Event& event)
{
	if (event.type == sf::Event::Closed)
	{
		render_window->close();
	}
	else if (event.type == sf::Event::Resized)
	{
		ui.resizeWindow({event.size.width, event.size.height});
	}

	ui.handleEvent(event);
	game.handleEvent(event, sf::Mouse::getPosition(*render_window), ui.getCanvasPtr());
}

void Application::draw()
{
	// note: order of drawing matters - there is no defined Z-order
	render_window->clear(constants::sfgui_background);

	render_window->setView(render_window->getDefaultView());
	ui.draw(*render_window);

	render_window->display();
}
