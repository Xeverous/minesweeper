#include <memory>
#include "Application.hpp"

/** @file */

// TODO: parse argc, argv?
int main()
{
    auto app = std::make_unique<Application>();
    int exit_code = app->run();

    // TODO: log exit code

    return exit_code;
}
