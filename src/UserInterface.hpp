#ifndef USERINTERFACE_HPP_
#define USERINTERFACE_HPP_
#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include <regex>
#include "Game.hpp"
#include "Global.hpp"

/**
 * @brief class for managing UI and game
 */
class UserInterface
{
public:
	/**
	 * @brief constructor
	 *
	 * @param game reference to game object that events will be invoked on
	 * @note game object is required to outlive instance of this class
	 */
	UserInterface(Game& game);
	/**
	 * @brief handle event from the system
	 *
	 * event is propagated across all UI elements and to the game
	 */
	void handleEvent(const sf::Event& event);
	/**
	 * @brief update UI and game
	 *
	 * @param time time passed between frames
	 *
	 * time is propagated across all UI elements and to the game
	 */
	void update(sf::Time time);
	/**
	 * @brief draw the UI and the game
	 *
	 * @param window window to draw to
	 */
	void draw(sf::RenderWindow& window);
	/**
	 * @brief prepare UI for different window size
	 *
	 * @param new_size new size of the window
	 */
	void resizeWindow(sf::Vector2u new_size);
	/**
	 * @brief get pointer to the canvas that game is being drawn to
	 *
	 * @return pointer to the canvas
	 */
	sfg::Canvas::Ptr getCanvasPtr() const;
	/**
	 * @brief trigger new game
	 *
	 * data will be taken from UI elements and passed to the game
	 *
	 * @note game is responsible for verifying values
	 */
	void newGame();

private:
	/**
	 * @defgroup BoundFunctions functions triggered when certain buttons/entries are modified
	 * @{
	 */
	void changeSizeX();
	void changeSizeY();
	void changeMinesAmount();
	void changeDensity();
	/** @} */

	/**
	 * @brief recalculates and sets new density
	 *
	 * Called when changed is:
	 * - mines_amount
	 * - size.x
	 * - size.y
	 */
	void refillDensity();
	/**
	 * @brief recalculates and sets new mines amount
	 *
	 * Called when changed is:
	 * - density
	 */
	void refillMinesAmount();
	/**
	 * @brief called when a difficulty button is clicked
	 */
	void setUpDifficulty(GameDifficulty game_difficulty);
	/**
	 * @brief pops out all difficulty buttons
	 */
	void deactivateDifficultyButtons();
	/**
	 * @brief utility function for converting input to integers
	 *
	 * used by @link BoundFunctions @endlink
	 *
	 * @return 0 in case of invalid string
	 */
	unsigned getValFromEntry(const sfg::Entry::Ptr& entry) const;

	/**
	 * @defgroup UserInterfaceCore core UI elements
	 * @{
	 */
	// Note: these 2 are scope-based local objects (not pointers)
	sfg::SFGUI m_sfgui;     /**< @brief SFGUI object; required by the library */
	sfg::Desktop m_desktop; /**< @brief virtual desktop to put widgets into */

	// Intentionally indented to show relations between UI elements
	sfg::Window::Ptr m_window;
		sfg::Box::Ptr m_box_main; // vertical
			sfg::Box::Ptr m_box_top; // horizontal

				sfg::Button::Ptr m_button_new_game;

				sfg::Frame::Ptr m_frame_difficulty;
					sfg::Box::Ptr m_box_difficulty; // horizontal
						sfg::ToggleButton::Ptr m_button_difficulty_easy;
						sfg::ToggleButton::Ptr m_button_difficulty_medium;
						sfg::ToggleButton::Ptr m_button_difficulty_hard;

				sfg::Frame::Ptr m_frame_size;
					sfg::Table::Ptr m_table_size;
						sfg::Label::Ptr m_label_size_x;
						sfg::Label::Ptr m_label_size_y;
						sfg::Entry::Ptr m_entry_size_x;
						sfg::Entry::Ptr m_entry_size_y;

				sfg::Frame::Ptr m_frame_density;
					sfg::Table::Ptr m_table_density;
						sfg::Label::Ptr m_label_mines;
						sfg::Label::Ptr m_label_density;
						sfg::Entry::Ptr m_entry_mines_amount;
						sfg::Entry::Ptr m_entry_density;
						sfg::Label::Ptr m_label_percent_sign;

				sfg::Frame::Ptr m_frame_current_game;
					sfg::Table::Ptr m_table_current_game;
						sfg::Label::Ptr m_label_flags_used_text;
						sfg::Label::Ptr m_label_time_playing_text;
						sfg::Label::Ptr m_label_flags_used_value;
						sfg::Label::Ptr m_label_time_playing_value;

			sfg::Canvas::Ptr m_canvas_game;
	/** @} */ // end of group

	std::regex regex_uint;     /**< @brief matches unsigned integer inputs @details leading 0s are allowed */
	std::regex regex_fraction; /**< @brief matches positive floating point or scientific notation inputs */

	Game& game;                /**< @brief referenced game object that functions will be called on */
	sf::Vector2u board_size;   /**< @brief size of the board retrieved from UI widgets */
	unsigned mines_amount;     /**< @brief amount of mines retrieved from UI widgets */
	/**
	 * @brief variable holding calculated mine density
	 *
	 * value range: (0, 1) - representing (0 - 100)%
	 */
	double density;
};

#endif /* USERINTERFACE_HPP_ */
