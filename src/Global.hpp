#ifndef GLOBAL_HPP_
#define GLOBAL_HPP_
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <random>

/** @file */
/** @brief free functions and global constants */

/**
 * @brief random number generator
 *
 * Mersenne Twister 19937
 * https://en.wikipedia.org/wiki/Mersenne_Twister
 */
extern std::mt19937 rng;

/**
 * @brief Scale given vector to desired size while keeping aspect ratio
 *
 * vector is resized so that:
 * - aspect ratio is preserved
 * - vector is as large as possible but fits into max_size
 *
 * Examples: (pairs of input => output vector)
 *
 *       20 x   40, 1000 x 3000 => 1000   x 2000
 *       20 x   40, 2000 x 3000 => 1500   x 3000
 *     1500 x 1000,   30 x   15 =>   22.5 x   15
 *     1000 x 3000,   20 x   40 =>   13.3 x   40
 *       30 x   15, 1500 x 1000 => 1500   x  750
 *     2000 x 3000,   20 x   40 =>   20   x   30
 */
sf::Vector2f scaleAndFit(const sf::Vector2f& vector, const sf::Vector2f& max_size);
/**
 * @brief Map a given point (relative to some object) to world's coordinates by using view applied to the object and it's size
 *
 * @param point point coords that should be mapped (relative to the object)
 * @param view view applied to the object
 * @param size size of the object
 * @return coordinations in the world's view system
 * @warning this function may return abnormal values such as NaN or INFINITY
 *
 * This function had to be implemented manually because:
 * - `sfg::Canvas` offers drawing by view, but does not have such function (in contrast to sf::RenderTarget)
 * - Inheriting from `sfg::Widget` provides only methods that pass global coordinates when an event occurs
 * - `sf::RenderTarget::mapPixelToCoords()` is not static, because it uses internal RenderTarget's size, which is private and
 *   inheriting from `sf::RenderTarget` would cause code bloat due to need of implementing unneeded virtual function bodies
 *
 * The body of this function is based on `sf::RenderTarget::mapPixelToCoords()`
 */
sf::Vector2f mapPixelToCoords(const sf::Vector2i& point, const sf::View& view, const sf::Vector2f& size);
/**
 * @brief Get viewport
 *
 * @return homogenous coordinates
 *
 * a helper function for mapPixelToCoords()
 * the body of this function is based on `sf::RenderTarget::getViewport()`
 */
sf::IntRect getViewport(const sf::View& view, const sf::Vector2f& size);
/** @brief enum describing game difficulty  */
enum class GameDifficulty : unsigned char
{
	easy, medium, hard, MAX_DIFFICULTIES
};
/** @brief struct holding data for each difficulty */
struct DifficultyData
{
	unsigned size_x, size_y;
	unsigned mines;
};
/** @brief holds all compile-time data */
namespace constants
{

const sf::Vector2u startup_window_size{1024, 768};

/**
 * @brief number of sides of the tile
 *
 * the most magic number in this program
 * maybe in the future I will make this game on hexagons?
 */
constexpr unsigned tile_sides = 4;
/**
 * @brief the size (in pixels) of signle tile in tileset texture file
 */
const sf::Vector2u tile_size{30, 30};
/**
 * @brief holds data for each difficulty level
 *
 * array should be accessed by casting enums of ::GameDifficulty
 */
const std::array
<
	DifficultyData,
	static_cast<std::size_t>(GameDifficulty::MAX_DIFFICULTIES)
> difficulty_data =
{{
	{ 9,  9, 10},
	{16, 16, 40},
	{30, 16, 99}
}};
/**
 * @brief color used to clear the screen
 *
 * this allows game field background to match with UI colors
 */
const sf::Color sfgui_background{70, 70, 70};

} // namespace constants

/** @brief holds all paths used in the application */
namespace paths
{

const std::string prefix_img = "img/";
const std::string tile_spritesheet = prefix_img + "mine_tiles.png";
const std::string prefix_font = "font/";
const std::string font = prefix_font + "KarmaticArcade.ttf";

} // namespace paths

#endif /* COMMON_H */
