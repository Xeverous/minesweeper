#include "Game.hpp"

#include <algorithm>
#include <cmath>
#include "Global.hpp"

Game::Game()
	: first_move(true),
	  total_mines(0),
	  flags_placed(0),
	  state(GameState::menu),
	  annoucement(Annoucement::none)
{
	tile_map.load(paths::tile_spritesheet);

	if (!font.loadFromFile(paths::font))
		throw std::runtime_error("Unable to load font: " + paths::font);

	text.setFont(font);
	text.setCharacterSize(72);
}

void Game::newGame(sf::Vector2u new_size, unsigned mines)
{
	if (new_size.x == 0)
	{
		setUpAnnoucement(Annoucement::invalid_width);
		return;
	}

	if (new_size.y == 0)
	{
		setUpAnnoucement(Annoucement::invalid_height);
		return;
	}

	// at least 1 tile must not be a mine
	if (mines >= new_size.x * new_size.y or mines == 0)
	{
		setUpAnnoucement(Annoucement::invalid_mines);
		return;
	}

	first_move = true;
	total_mines = mines;
	flags_placed = 0;
	state = GameState::menu;
	time_playing = sf::Time::Zero;
	tile_map.reset(new_size, total_mines);
}

bool Game::isFirstMove() const
{
	return first_move;
}

bool Game::isFinished() const
{
	return state == GameState::won or state == GameState::lost;
}

bool Game::isPlaying() const
{
	return state == GameState::playing;
}

void Game::update(sf::Time time)
{
	if (isPlaying())
		time_playing += time;

	annoucement_time -= time;
	if (annoucement_time <= sf::Time::Zero)
	{
		annoucement_time = sf::Time::Zero;
		annoucement = Annoucement::none;
	}

	tile_map.update(time);
}

void Game::handleEvent(const sf::Event& event, sf::Vector2i global_mouse_position, sfg::Canvas::Ptr canvas)
{
	sf::Vector2i canvas_point(
		global_mouse_position.x - canvas->GetAbsolutePosition().x,
		global_mouse_position.y - canvas->GetAbsolutePosition().y
	);

	sf::Vector2f internal_position = mapPixelToCoords(
		canvas_point,
		tile_map_view,
		{canvas->GetAllocation().width, canvas->GetAllocation().height}
	);

	handleEvent(event, internal_position);
}

void Game::handleEvent(const sf::Event& event, sf::Vector2f mouse_position)
{
	if (event.type == sf::Event::MouseButtonPressed)
	{
		// ignore events with abnormal mouse positions
		if ( !(std::isfinite(mouse_position.x) and std::isfinite(mouse_position.y)) )
			return;

		sf::Vector2u position = tile_map.getTilePosition(mouse_position);
		if (!tile_map.isPositionValid(position))
			return;

		if (isFinished())
			return;

		state = GameState::playing;

		if (event.mouseButton.button == sf::Mouse::Left or event.mouseButton.button == sf::Mouse::Middle)
		{
			startRevealing(position);
		}
		else if (event.mouseButton.button == sf::Mouse::Right)
		{
			flagAndCheck(position);
		}
	}
}

void Game::startRevealing(sf::Vector2u position)
{
	Tile& tile = tile_map.getTileByPosition(position);

	if (tile.isFlagged())
		return;

	if (tile.isMine())
	{
		if (!isFirstMove())
		{
			setUpGameLoss();
			return;
		}

		tile_map.swapTileToNonMine(position);
	}
	else if (tile.isOnlyNumber() and tile.isRevealed())
	{
		if (tile_map.isTileSatisfied(position))
		{
			tile_map.recursivelyRevealNeighbours(position);

			if (tile_map.isMineRevealed())
				setUpGameLoss();
		}
	}

	tile_map.recursivelyReveal(position);
	first_move = false;
}

void Game::flagAndCheck(sf::Vector2u position)
{
	Tile& tile = tile_map.getTileByPosition(position);

	if (flags_placed == total_mines and tile.isUnmarked())
	{
		setUpAnnoucement(Annoucement::out_of_flags);
		return;
	}

	tile.flag();

	if (tile.isFlagged())
	{
		flags_placed++;

		if (tile_map.verifyFlagsCorrectness())
		{
			setUpAnnoucement(Annoucement::won);
			state = GameState::won;
			tile_map.revealAllTiles();
		}
	}
	else if (tile.isQuestioned())
	{
		flags_placed--;
	}
}

void Game::draw(sfg::Canvas::Ptr canvas)
{
	sf::Vector2f canvas_size(canvas->GetAllocation().width, canvas->GetAllocation().height);

	// draw tile map
	recalculateTileMapView(canvas_size);
	canvas->SetView(tile_map_view);
	tile_map.draw(canvas, sf::RenderStates());

	// draw annoucement text
	if (annoucement != Annoucement::none)
	{
		const sf::Vector2u& s = constants::startup_window_size;
		sf::Vector2f size = scaleAndFit(canvas_size, sf::Vector2f(s.x, s.y));

		sf::View text_view;
		text_view.setSize(size);
		text_view.setCenter(size / 2.0f);
		text_view.setViewport({0, 0, 1.0f, 1.0f});
		canvas->SetView(text_view);

		text.setPosition(size / 2.0f);
		canvas->Draw(text);
	}
}

void Game::recalculateTileMapView(sf::Vector2f canvas_size)
{
	// stretch game's size as much as possible
	sf::Vector2f game_stretched_size = scaleAndFit(tile_map.getScreenRequisition(), canvas_size);

	// count start point and length of each axis for viewport (in pixels)
	float x_start  = (canvas_size.x - game_stretched_size.x) / 2.0f;
	float y_start  = (canvas_size.y - game_stretched_size.y) / 2.0f;
	float x_length = game_stretched_size.x;
	float y_length = game_stretched_size.y;

	// calculate positions relative to canvas size
	tile_map_view.setViewport({
		x_start  / canvas_size.x,
		y_start  / canvas_size.y,
		x_length / canvas_size.x,
		y_length / canvas_size.y
	});

	/*
	 * set view's size to match game units
	 * this will allow game to treat each point equally,
	 * like the render window has exact size of the game
	 */
	sf::Vector2f size = tile_map.getScreenRequisition();
	tile_map_view.setSize(size);
	tile_map_view.setCenter(size / 2.0f);
}

void Game::setUpGameLoss()
{
	setUpAnnoucement(Annoucement::lost);
	state = GameState::lost;
	tile_map.revealAllTiles();
}

std::string Game::getPlayingTime() const
{
	if (isFirstMove())
		return "-";

	if (isFinished())
		return std::to_string(time_playing.asSeconds()) + "s";

	return std::to_string(static_cast<int>(time_playing.asSeconds())) + "s";
}

std::string Game::getFlagsStatus() const
{
	if (isFirstMove())
		return "-";
	else
		return std::to_string(flags_placed) + " / " + std::to_string(total_mines);
}

void Game::setUpAnnoucement(Annoucement a)
{
	annoucement = a;
	annoucement_time = sf::seconds(2.0f);

	text.setString(getAnnoucementString(a));
	text.setOrigin({text.getLocalBounds().width / 2.0f, text.getLocalBounds().height / 2.0f});
}

std::string Game::getAnnoucementString(Annoucement a) const
{
	switch (a)
	{
		case Annoucement::won:            return "You won!";
		case Annoucement::lost:           return "You lost!";
		case Annoucement::invalid_width:  return "invalid width";
		case Annoucement::invalid_height: return "invalid height";
		case Annoucement::invalid_mines:  return "invalid amount\nof mines";
		case Annoucement::out_of_flags:   return "no more flags";
		default: return "";
	}
}
