#include "TileMap.hpp"

#include <exception>
#include "Global.hpp"

TileMap::TileMap()
{
	vertices.setPrimitiveType(sf::Quads);
}

void TileMap::load(const std::string& tileset_path)
{
	if (!texture.loadFromFile(tileset_path))
		throw std::runtime_error("Unable to load texture: " + tileset_path);
}

void TileMap::reset(sf::Vector2u new_size, unsigned total_mines)
{
	board_size = new_size;

	unsigned new_container_size = new_size.x * new_size.y;
	vertices.resize(new_container_size * constants::tile_sides);

	tiles.clear();
	tiles.shrink_to_fit();
	tiles.reserve(new_container_size);

	// place mines
	unsigned i;
	for (i = 0; i < total_mines; i++)
		tiles.emplace_back(true);

	// place empty tiles
	for (; i < new_size.x * new_size.y; i++)
		tiles.emplace_back(false);

	std::shuffle(tiles.begin(), tiles.end(), rng);

	recalculateScreenPositions();
	recalculateTexturePositions();
	calculateMineNeighbours();
}

void TileMap::update(sf::Time time)
{
	// no animations so far, so just supress the warning
	(void)time;
}

void TileMap::recalculateScreenPositions()
{
	for (unsigned y = 0; y < board_size.y; y++)
		for (unsigned x = 0; x < board_size.x; x++)
		{
			// get a pointer to the current tile's quad
			sf::Vertex* quad = getVertexByPosition({x, y});

			// define its 4 corners
			const auto& s = constants::tile_size;
			quad[0].position = sf::Vector2f( x      * s.x,  y      * s.y);
			quad[1].position = sf::Vector2f((x + 1) * s.x,  y      * s.y);
			quad[2].position = sf::Vector2f((x + 1) * s.x, (y + 1) * s.y);
			quad[3].position = sf::Vector2f( x      * s.x, (y + 1) * s.y);
		}
}

void TileMap::recalculateTexturePositions()
{
	for (unsigned y = 0; y < board_size.y; y++)
		for (unsigned x = 0; x < board_size.x; x++)
		{
			// get the current tile number
			unsigned image_index = getTileByPosition({x, y}).getImageIndex();

			// find its position in the tileset texture
			// https://en.wikipedia.org/wiki/UV_mapping
			const auto& s = constants::tile_size;
			unsigned tu = image_index % (texture.getSize().x / s.x);
			unsigned tv = image_index / (texture.getSize().x / s.x);

			// get a pointer to the current tile's quad
			sf::Vertex* quad = getVertexByPosition({x, y});

			// define its 4 texture coordinates
			quad[0].texCoords = sf::Vector2f( tu      * s.x,  tv      * s.y);
			quad[1].texCoords = sf::Vector2f((tu + 1) * s.x,  tv      * s.y);
			quad[2].texCoords = sf::Vector2f((tu + 1) * s.x, (tv + 1) * s.y);
			quad[3].texCoords = sf::Vector2f( tu      * s.x, (tv + 1) * s.y);
		}
}

sf::Vector2f TileMap::getScreenRequisition() const
{
	return sf::Vector2f(
		board_size.x * constants::tile_size.x,
		board_size.y * constants::tile_size.y
	);
}

void TileMap::draw(sfg::Canvas::Ptr canvas, sf::RenderStates states)
{
	recalculateTexturePositions();

	// abort drawing empty board
	if (vertices.getVertexCount() == 0)
		return;

	// apply the transform and texture
	states.transform *= getTransform();
	states.texture = &texture;

	canvas->Draw(&vertices[0], vertices.getVertexCount(), vertices.getPrimitiveType(), states);
}

sf::Vector2u TileMap::getTilePosition(const sf::Vector2f& mouse_pos) const
{
	return {
		static_cast<unsigned>(mouse_pos.x) / constants::tile_size.x,
		static_cast<unsigned>(mouse_pos.y) / constants::tile_size.y
	};
}

bool TileMap::isPositionValid(const sf::Vector2u& position) const
{
	return position.x < board_size.x and position.y < board_size.y;
}

Tile& TileMap::getTileByPosition(const sf::Vector2u& position)
{
	return tiles[board_size.x * position.y + position.x];
}

const Tile& TileMap::getTileByPosition(const sf::Vector2u& position) const
{
	return tiles[board_size.x * position.y + position.x];
}

sf::Vertex* TileMap::getVertexByPosition(const sf::Vector2u& position)
{
	return &vertices[(board_size.x * position.y + position.x) * constants::tile_sides];
}

bool TileMap::verifyFlagsCorrectness() const
{
	for (const auto& tile : tiles)
		if (!tile.isCorrectlyFlagged())
			return false;

	return true;
}

bool TileMap::isMineRevealed() const
{
	for (const auto& tile : tiles)
		if (tile.isRevealed() and tile.isMine())
			return true;

	return false;
}

bool TileMap::isTileSatisfied(const sf::Vector2u& position) const
{
	unsigned neighbour_flags = countForAllNeighbours
	(
		position,
		[this](const sf::Vector2u& position) -> bool
		{
			if (!isPositionValid(position))
				return false;

			return getTileByPosition(position).isFlagged();
		}
	);

	return neighbour_flags == getTileByPosition(position).getNeighbourMinesAmount();
}

void TileMap::revealAllTiles()
{
	for (auto&& tile : tiles)
		tile.reveal(false); // force reveal - game has finished
}

void TileMap::recursivelyReveal(const sf::Vector2u& position)
{
	if (!isPositionValid(position))
		return;

	Tile& tile = getTileByPosition(position);

	if (tile.isRevealed())
		return;

	tile.reveal();

	if (tile.isSafe())
		recursivelyRevealNeighbours(position);
}

void TileMap::recursivelyRevealNeighbours(const sf::Vector2u& position)
{
	doForEachNeighbour
	(
		position,
		[this](const sf::Vector2u& position) -> void { recursivelyReveal(position); }
	);
}

void TileMap::swapTileToNonMine(sf::Vector2u position)
{
	std::vector<unsigned> non_mine_indexes;
	non_mine_indexes.reserve(tiles.size());

	// save all non-mine indexes
	for (unsigned i = 0; i < tiles.size(); i++)
		if (!tiles[i].isMine())
			non_mine_indexes.push_back(i);

	// pick a random index
	std::uniform_int_distribution<unsigned> dist(0, non_mine_indexes.size() - 1);
	unsigned choosen_tile_index = non_mine_indexes[dist(rng)];

	// swap mine with random non-mine
	std::swap(tiles[choosen_tile_index], getTileByPosition(position));

	// converting 1D index into 2D position
	sf::Vector2u swapped_position{choosen_tile_index % board_size.x, choosen_tile_index / board_size.x};

	// recalculate neighbour data - now it's different
	calculateMineNeighbours(position);
	calculateMineNeighbours(swapped_position);

	auto lambda = [this](const sf::Vector2u& position) -> void
	{
		// call mines calculation but first check if position is valid
		if (!isPositionValid(position))
			return;

		calculateMineNeighbours(position);
	};

	doForEachNeighbour(position,         lambda);
	doForEachNeighbour(swapped_position, lambda);
}

void TileMap::calculateMineNeighbours()
{
	for (unsigned x = 0; x < board_size.x; x++)
		for (unsigned y = 0; y < board_size.y; y++)
			calculateMineNeighbours({x, y});
}

void TileMap::calculateMineNeighbours(const sf::Vector2u& position)
{
	getTileByPosition(position).setMineNeighbours
	(
		countForAllNeighbours
		(
			position,
			[this](const sf::Vector2u& position) -> bool
			{
				if (!isPositionValid(position))
					return false;

				return getTileByPosition(position).isMine();
			}
		)
	);
}
